# alias_method

An efficient way of sampling a large non-uniform discrete distribution. See
[Alias Method](https://en.wikipedia.org/wiki/Alias_method).

### Usage

```text
usage: sample.py PR_CSV_FILE RANDOM_SEED

Output will be one number per line on stdout.

positional arguments:
 PR_CSV_FILE            path to csv file containing e.g.
1,0.2
2,0.4
7,0.3
8,0.03
9,0.01
10,0.05
11,0.01
 RANDOM_SEED            integer seed for reproducible runs
```

### Notes

This runs easily under `pypy3` because of few dependencies, and is fast (e.g. a
distribution with 10M entries will load and finish table generation in <30sec).

Sampling speed is theoretically O(1) and also independent of distribution size
in practice, except for the in- memory tables being size O(n) and thus
exhibiting cache misses for large enough n.

Input CSV is not checked for validity or that the probabilities sum to 1.

The precalculated tables are not optimized to reduce likelihood of consulting
the alias table. Sampling is still O(1).
