import sys
import math


def load_pr(pr_csv_path):
    pr_dict = {}
    with open(pr_csv_path) as f:
        for line in f:
            number, probability = line.strip().split(',')
            number = int(number)
            probability = float(probability)

            if number in pr_dict:
                raise RuntimeError('duplicate number present in input file')

            pr_dict[number] = probability

    return pr_dict


# https://en.wikipedia.org/wiki/Alias_method
def generate_alias_table(pr_dict):
    num_values = len(pr_dict)
    keys = list(pr_dict.keys())
    pr_table = list(map(lambda x: num_values*x, pr_dict.values()))
    alias_table = [None]*num_values

    overfull = []
    underfull = []
    full = []
    for i, val in enumerate(pr_table):
        if val > 1.:
            overfull.append(i)
        elif val < 1.:
            underfull.append(i)
        elif val == 1.:
            full.append(i)
        else:
            assert False

    while len(full) < num_values:
        if not (overfull and underfull):
            for val in (overfull+underfull):
                pr_table[val] = 1.
            break

        alias_table[underfull[-1]] = overfull[-1]
        pr_table[overfull[-1]] -= (1. - pr_table[underfull[-1]])

        full.append(underfull[-1])
        underfull.pop()

        if pr_table[overfull[-1]] < 1.:
            underfull.append(overfull[-1])
            overfull.pop()
        elif pr_table[overfull[-1]] == 1.:
            full.append(overfull[-1])
            overfull.pop()
        else:
            pass

    return keys, pr_table, alias_table


# https://en.wikipedia.org/wiki/Wichmann%E2%80%93Hill
class Random:

    def seed(self, a):
        a, x = divmod(a, 30268)
        a, y = divmod(a, 30306)
        a, z = divmod(a, 30322)
        self._seed = int(x)+1, int(y)+1, int(z)+1


    def random(self):
        x, y, z = self._seed
        x = (171 * x) % 30269
        y = (172 * y) % 30307
        z = (170 * z) % 30323
        self._seed = x, y, z
        return (x/30269.0 + y/30307.0 + z/30323.0) % 1.0


def sample(keys, pr_table, alias_table):
    n = len(keys)
    x = random.random()
    i = math.floor(n*x)
    y = n*x - float(i)
    if y < pr_table[i]:
        return keys[i]
    else:
        return keys[alias_table[i]]


def main():
    pr_dict = load_pr(pr_csv_path)

    keys, pr_table, alias_table = generate_alias_table(pr_dict)

    while True:
        val = sample(keys, pr_table, alias_table)
        print(val)


if __name__ == '__main__':
    try:
        pr_csv_path = sys.argv[1]
        seed = int(sys.argv[2])
    except:
        print('''
usage: sample.py PR_CSV_FILE RANDOM_SEED

Output will be one number per line on stdout.

positional arguments:
 PR_CSV_FILE            path to csv file containing e.g.
1,0.2
2,0.4
7,0.3
8,0.03
9,0.01
10,0.05
11,0.01
 RANDOM_SEED            integer seed for reproducible runs
''')
        sys.exit(1)
    random = Random()
    random.seed(seed)

    main()
